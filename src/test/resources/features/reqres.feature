#Author: your.email@your.domain.com
@ReqRes
Feature: API AUTOMATION USING RESTASSURED
  I want to automate the api testing using rest assured.

  @tag1
  Scenario: Validating the get method
    Given the apis are running for "https://reqres.in"
    When user performs get request to "/api/users?page=2"
    And performs a request
    Then response code should be "200"

    
    @tag2
  Scenario: Validating the post method
    Given the apis are running for "https://reqres.in"