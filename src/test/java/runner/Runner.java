package runner;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber",
		"json:target/cucumber.json" }, 
		features = "src/test/resources/features", 
		glue = "stepDefination", 
		tags = {"@tag1"},
		monochrome=true

)

public class Runner {

}








