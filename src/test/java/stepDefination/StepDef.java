package stepDefination;

import java.util.Map;

import org.junit.Assert;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import resources.ResponseHolder;

import static io.restassured.RestAssured.*;

public class StepDef {

	Response response;
	String url;
	String lastname;
	Map<String, Object> responseMap;
	Map<String, String> query;
	

	
	
	@Given("^the apis are running for \"([^\"]*)\"$")
	public void the_apis_are_running_for(String url) {
		this.url = url;
		response=given().when().get(url);
		Assert.assertEquals(200, response.getStatusCode());
	}

	@When("^user performs get request to \"([^\"]*)\"$")
	public void user_performs_get_request_to(String api_url) {
		this.url=this.url+api_url;
	}

	@And("performs a request")
	public void performs_a_request() {
		if(query == null) {
			response = given().when().get(this.url);
		}
		else {
			response=given().queryParams(query).when().get(this.url);
		}
		ResponseHolder.setResponse(response);
	}

	@Then("^response code should be \"([^\"]*)\"$")
	public void response_code_should_be(int code) {
		Assert.assertEquals(code, ResponseHolder.getResponseCode());
	}
}
